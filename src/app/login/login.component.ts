import { Component, OnInit } from '@angular/core';
import { UsersService } from './../users/users.service';
import {FormGroup , FormControl} from '@angular/forms';
import {Router} from '@angular/router';

@Component({
  selector: 'login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  invalid = false;
token;
  loginform = new FormGroup({
    user:new FormControl(),
    password:new FormControl()
  });

  login(){
    this.service.login(this.loginform.value).subscribe(response=>{
      this.token = localStorage.getItem('token');
      console.log(this.token);
      this.router.navigate(['/']);
    } , error=>{
      this.invalid = true;
    })
  }

  logout(){
    this.token = localStorage.removeItem('token');
    console.log(this.token);
    this.invalid = false;
  }

  constructor(private service:UsersService, private router:Router) { }

  ngOnInit() {
  }

}