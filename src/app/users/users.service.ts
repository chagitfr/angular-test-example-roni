import { Injectable } from '@angular/core';
import { Http , Headers } from '@angular/http';
import { HttpParams } from '@angular/common/http';
import 'rxjs/Rx';

@Injectable()
export class UsersService {
  http:Http;

  getUsers(){
    let token='?token='+localStorage.getItem('token');
    //var params = new HttpParams().append('token',token);
    /*let options = {
      headers: new Headers({
        'content-type': 'application/x-www-form-urlencoded',
        'Authorization':'Bearer '+token
      }
    )};*/
    return this.http.get('http://localhost/angular/testexample/slim/users'+token);
  }

  updateUser(id,user){
    let token = localStorage.getItem('token');
    let options = {
      headers: new Headers({
        'content-type': 'application/x-www-form-urlencoded',
        'Authorization':'Bearer '+token
      }
    )};
    var params = new HttpParams().append('name',user.name).append('phonenumber',user.phonenumber).append('token',token);
    return this.http.put('http://localhost/angular/testexample/slim/users/'+id, params.toString(), options);      
  }

  postUser(user){
    let token = localStorage.getItem('token');
    let options = {
      headers: new Headers({
        'content-type': 'application/x-www-form-urlencoded',
        //'Authorization':'Bearer '+token
      }
    )};
    var params = new HttpParams().append('name',user.name).append('phonenumber',user.phonenumber).append('token',token);
    return this.http.post('http://localhost/angular/testexample/slim/users',params.toString(), options);     
  }

  deleteUser(key)
  {
    let token=localStorage.getItem('token');
    console.log(key); 
    console.log(token);
    return this.http.delete('http://localhost/angular/testexample/slim/users/:'+ key + '/:' + token);
  }

  /*
  $app->delete('/users/:{user_id}/:{token}', function($request, $response,$args){
    $token = $args['token'];
    if($token=='12345')
      { 
        $user = User::find($args['user_id']);
        $user->delete(); 
      if($user->exists){
        return $response->withStatus(400);
      }
      else{
        return $response->withStatus(200);
      }
      }
    else{
        return $response->withStatus(404)->withJson($payload);
      }
    });
*/


  //Login
  login(credentials){
    let options = {
      headers: new Headers({
        'content-type':'application/x-www-form-urlencoded'
      })
    }
    
    let params = new HttpParams().append('user',credentials.user).append('password',credentials.password);
  
    return this.http.post('http://localhost/angular/testexample/slim/auth', params.toString(), options).map(response=>{
        let token = response.json().token; //הטוקן מצד השרת
        if(token) 
          localStorage.setItem('token' , token);

    });
  }

  constructor(http:Http) { 
    this.http = http;
  }

}
